locals {
  az_count = length(var.availability_zones)
}

resource "aws_subnet" "public" {
  count                   = local.az_count
  availability_zone       = var.availability_zones[count.index]
  cidr_block              = "${var.subnet_prefix}.${var.public_start_ip + count.index}.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.default.id

  tags = merge(module.label.tags, { Name = "Public-${var.availability_zones[count.index]}" }, { Tier = "Public" })
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.default.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default[0].id
  }

  tags = merge(module.label.tags, { Name = "public_subnet_route" })
}

resource "aws_route_table_association" "public" {
  count          = local.az_count
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public.id
}

resource "aws_network_acl" "public" {
  vpc_id     = aws_vpc.default.id
  subnet_ids = aws_subnet.public.*.id

  egress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
  }

  ingress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
  }

  tags = merge(module.label.tags, { Name = "Public-ACL-${var.availability_zones[0]}" }, { Tier = "Public" })
}

resource "aws_subnet" "private" {
  count             = local.az_count
  availability_zone = var.availability_zones[count.index]
  cidr_block        = "${var.subnet_prefix}.${var.private_start_ip + count.index}.0/24"
  vpc_id            = aws_vpc.default.id

  tags = merge(module.label.tags, { Name = "Private-${var.availability_zones[count.index]}" }, { Tier = "Private" })
}

resource "aws_default_route_table" "private" {
  count = var.enable_nat_gateway == true ? 1 : 0

  default_route_table_id = aws_vpc.default.default_route_table_id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.default[0].id
  }

  tags = merge(module.label.tags, { Name = "private_subnet_route" })
}

resource "aws_route_table_association" "private" {
  count          = local.az_count
  route_table_id = aws_vpc.default.main_route_table_id
  subnet_id      = aws_subnet.private[count.index].id
}

resource "aws_network_acl" "private" {
  vpc_id     = aws_vpc.default.id
  subnet_ids = aws_subnet.private.*.id

  egress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
  }

  ingress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
  }

  tags = merge(module.label.tags, { Name = "Private-ACL-${var.availability_zones[0]}" }, { Tier = "Private" })
}
