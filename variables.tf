variable "project" {
  description = "Name of project your app belongs to."
  type        = string
}

variable "environment" {
  description = "Environment where app should be deployed like dev, pre, qa or prod."
  type        = string
}

variable "cidr_block" {
  type        = string
  description = "The CIDR for the VPC"
}

variable "instance_tenancy" {
  type        = string
  description = "A tenancy option for instances launched into the VPC"
  default     = "default"
}

variable "enable_dns_support" {
  type        = bool
  description = "A boolean flag to enable/disable DNS support in the VPC"
  default     = true
}

variable "enable_dns_hostnames" {
  type        = bool
  description = "A boolean flag to enable/disable DNS hostnames in the VPC"
  default     = true
}

variable "assign_generated_ipv6_cidr_block" {
  type        = bool
  description = "Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC"
  default     = true
}

variable "tags" {
  type        = map(string)
  description = "Additional tags"
  default     = {}
}

variable "enable_nat_gateway" {
  type        = bool
  description = "A boolean flag to enable/disable NAT Gateway creation"
  default     = false
}

variable "enable_internet_gateway" {
  type        = bool
  description = "A boolean flag to enable/disable Internet Gateway creation"
  default     = true
}

variable "enable_default_security_group" {
  type        = bool
  description = "A boolean flag to enable/disable custom and restricive inbound/outbound rules for the default VPCs SG"
  default     = true
}

variable "additional_cidr_blocks" {
  type        = list(string)
  description = "A list of additional IPv4 CIDR blocks to associate with the VPC"
  default     = null
}

variable "availability_zones" {
  type        = list(any)
  description = "AWS AVailability Zones"
}

variable "public_start_ip" {
  type        = string
  description = "Start ip of public subnets"
}

variable "private_start_ip" {
  type        = string
  description = "Start ip of private subnets"
}

variable "subnet_prefix" {
  type        = string
  description = "Prefix for setup of subnets"
}
