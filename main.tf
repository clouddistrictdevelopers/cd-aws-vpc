locals {
  enable_default_security_group  = var.enable_default_security_group ? 1 : 0
  enable_internet_gateway        = var.enable_internet_gateway ? 1 : 0
  enable_nat_gateway             = var.enable_nat_gateway ? 1 : 0
  additional_cidr_blocks_defined = var.additional_cidr_blocks != null ? true : false
  additional_cidr_blocks         = local.additional_cidr_blocks_defined ? var.additional_cidr_blocks : []
}

module "label" {
  source = "bitbucket.org/clouddistrictdevelopers/cd-aws-label?ref=v0.0.3"

  environment = var.environment
  project     = var.project
  delimiter   = "-"

  tags = var.tags
}

resource "aws_vpc" "default" {
  cidr_block                       = var.cidr_block
  instance_tenancy                 = var.instance_tenancy
  enable_dns_hostnames             = var.enable_dns_hostnames
  enable_dns_support               = var.enable_dns_support
  assign_generated_ipv6_cidr_block = var.assign_generated_ipv6_cidr_block
  tags                             = module.label.tags
}

resource "aws_vpc_ipv4_cidr_block_association" "default" {
  for_each   = toset(local.additional_cidr_blocks)
  vpc_id     = aws_vpc.default.id
  cidr_block = each.key
}

resource "aws_default_security_group" "default" {
  count  = local.enable_default_security_group
  vpc_id = aws_vpc.default.id

  tags = merge(module.label.tags, { Name = "Default Security Group" })
}

resource "aws_internet_gateway" "default" {
  count  = local.enable_internet_gateway
  vpc_id = aws_vpc.default.id
  tags   = module.label.tags
}

resource "aws_eip" "nat_eip" {
  count      = local.enable_nat_gateway
  vpc        = true
  depends_on = [aws_internet_gateway.default]

  tags = module.label.tags
}

resource "aws_nat_gateway" "default" {
  count         = local.enable_nat_gateway
  allocation_id = element(aws_eip.nat_eip.*.id, 0)
  subnet_id     = element(aws_subnet.public.*.id, 0)
  depends_on    = [aws_internet_gateway.default]

  tags = module.label.tags
}
