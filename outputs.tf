output "igw_id" {
  value       = aws_internet_gateway.default.*.id
  description = "The ID of the Internet Gateway"
}

output "vpc_id" {
  value       = aws_vpc.default.id
  description = "The ID of the VPC"
}

output "vpc_cidr_block" {
  value       = aws_vpc.default.cidr_block
  description = "The CIDR block of the VPC"
}

output "vpc_main_route_table_id" {
  value       = aws_vpc.default.main_route_table_id
  description = "The ID of the main route table associated with this VPC"
}

output "vpc_default_network_acl_id" {
  value       = aws_vpc.default.default_network_acl_id
  description = "The ID of the network ACL created by default on VPC creation"
}

output "vpc_default_security_group_id" {
  value       = aws_vpc.default.default_security_group_id
  description = "The ID of the security group created by default on VPC creation"
}

output "vpc_default_route_table_id" {
  value       = aws_vpc.default.default_route_table_id
  description = "The ID of the route table created by default on VPC creation"
}

output "vpc_ipv6_association_id" {
  value       = aws_vpc.default.ipv6_association_id
  description = "The association ID for the IPv6 CIDR block"
}

output "vpc_ipv6_cidr_block" {
  value       = aws_vpc.default.ipv6_cidr_block
  description = "The IPv6 CIDR block"
}

output "vpc_additional_cidr_blocks" {
  description = "A list of the additional IPv4 CIDR blocks associated with the VPC"
  value = [
    for i in aws_vpc_ipv4_cidr_block_association.default :
    i.cidr_block
    if local.additional_cidr_blocks_defined
  ]
}

output "vpc_additional_cidr_blocks_to_association_ids" {
  description = "A map of the additional IPv4 CIDR blocks to VPC CIDR association IDs"
  value = {
    for i in aws_vpc_ipv4_cidr_block_association.default :
    i.cidr_block => i.id
    if local.additional_cidr_blocks_defined
  }
}

output "vpc_az_private_subnet_ids" {
  value = zipmap(
    var.availability_zones,
    aws_subnet.private.*.id
  )
  description = "Map of AZ names to private subnet IDs"
}

output "vpc_az_public_subnet_ids" {
  value = zipmap(
    var.availability_zones,
    aws_subnet.public.*.id
  )
  description = "Map of AZ names to public subnet IDs"
}

output "vpc_public_subnet_ids" {
  value       = aws_subnet.public.*.id
  description = "IDs of the created public subnets"
}

output "vpc_private_subnet_ids" {
  value       = aws_subnet.private.*.id
  description = "IDs of the created private subnets"
}

# output "vpc_az_route_table_ids" {
#   value = zipmap(
#     var.availability_zones,
#     coalescelist(aws_default_route_table.private.*.id, aws_route_table.public.*.id),
#   )
#   description = " Map of AZ names to Route Table IDs"
# }

# output "vpc_az_ngw_ids" {
#   value = zipmap(
#     var.availability_zones,
#     coalescelist(aws_nat_gateway.default.*.id, local.dummy_az_ngw_ids),
#   )
#   description = "Map of AZ names to NAT Gateway IDs (only for public subnets)"
# }

output "vpc_az_private_subnet_arns" {
  value = zipmap(
    var.availability_zones,
    aws_subnet.private.*.arn
  )
  description = "Map of AZ names to private subnet ARNs"
}

output "vpc_az_public_subnet_arns" {
  value = zipmap(
    var.availability_zones,
    aws_subnet.public.*.arn
  )
  description = "Map of AZ names to public subnet ARNs"
}

# Dummy list of NAT Gateway IDs to use in the outputs for private subnets and when `nat_gateway_enabled=false` for public subnets
# Needed due to Terraform limitation of not allowing using conditionals with maps and lists
locals {
  dummy_az_ngw_ids = slice(
    [
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
    ],
    0,
    length(var.availability_zones),
  )
}
